GeoIP-lite
==========

A native NodeJS API for IP geolocation data from db-ip.com.

This product includes IP geolocation data created by db-ip.com.

synopsis
--------

```javascript
var geoip = require('geoip-lite');

var ip = "207.97.227.239";
var geo = geoip.lookup(ip);

console.log(geo);
{ range: [ 1234567890, 1234567990 ],
  country: 'US',
  region: 'KS',
  city: 'Topeka',
  ll: [ 30.1234, -100.123 ],
  metro: 789,
  zip: 55555 }
```

installation
------------
### 1. get the library

### 2. update the datafiles (optional)

Run `npm run-script updatedb` to update the data files.

**NOTE** that this requires a lot of RAM.  It is known to fail on on a Digital Ocean or AWS micro instance.
There are no plans to change this.  `geoip-lite` stores all data in RAM in order to be fast.

API
---

geoip-lite is completely synchronous.  There are no callbacks involved.  All blocking file IO is done at startup time, so all runtime
calls are executed in-memory and are fast.  Startup may take up to 200ms while it reads into memory and indexes data files.

### Looking up an IP address ###

If you have an IP address in dotted quad notation, IPv6 colon notation, or a 32 bit unsigned integer (treated
as an IPv4 address), pass it to the `lookup` method.  Note that you should remove any `[` and `]` around an
IPv6 address before passing it to this method.

```javascript
var geo = geoip.lookup(ip);
```

If the IP address was found, the `lookup` method returns an object with the following structure:

```javascript
{
   range: [ <low bound of IP block>, <high bound of IP block> ],
   country: 'XX',                 // 2 letter ISO-3166-1 country code
   region: 'RR',                  // 2 character region code.  For US states this is the 2 letter
                                  // ISO-3166-2 subcountry code for other countries, this is the
                                  // FIPS 10-4 subcountry code
   city: "City Name",             // This is the full city name
   ll: [<latitude>, <longitude>], // The latitude and longitude of the city
   metro: <metro code>,           // Metro code
   zip: <postal code>             // Postal code (IPv4 only)
}
```

The actual values for the `range` array depend on whether the IP is IPv4 or IPv6 and should be
considered internal to `geoip-lite`.  To get a human readable format, pass them to `geoip.pretty()`

If the IP address was not found, the `lookup` returns `null`

### Pretty printing an IP address ###

If you have a 32 bit unsigned integer, or a number returned as part of the `range` array from the `lookup` method,
the `pretty` method can be used to turn it into a human readable string.

```javascript
    console.log("The IP is %s", geoip.pretty(ip));
```

This method returns a string if the input was in a format that `geoip-lite` can recognise, else it returns the
input itself.

### Start and stop watching for data updates ###

If you have a server running `geoip-lite`, and you want to update its geo data without a restart, you can enable
the data watcher to automatically refresh in-memory geo data when a file changes in the data directory.

```javascript
geoip.startWatchingDataUpdate();
```

This tool can be used with `npm run-script updatedb` to periodically update geo data on a running server.


Built-in Updater
----------------

This package contains an update script that can pull the files from db-ip.com and handle the conversion from CSV.
A npm script alias has been setup to make this process easy. Please keep in mind this requires internet.

```shell
npm run-script updatedb
```

You can also run it by doing:

```bash
node ./node_modules/geoip-lite/scripts/updatedb.js
```

Or, if you really want, run the update once by `require('geoip-lite/scripts/updatedb.js')`.

Caveats
-------

Also note that on occassion, the library may take up to 5 seconds to load into memory.  This is largely dependent on
how busy your disk is at that time.  It can take as little as 200ms on a lightly loaded disk.  This is a one time
cost though, and you make it up at run time with very fast lookups.

References
----------
  - <a href="http://en.wikipedia.org/wiki/ISO_3166">ISO 3166 (1 & 2) codes</a>
  - <a href="http://en.wikipedia.org/wiki/List_of_FIPS_region_codes">FIPS region codes</a>

Copyright
---------

`geoip-lite` is Copyright 2011-2018 Philip Tellis <philip@bluesmoon.info> and the latest version of the code is
available at https://github.com/bluesmoon/node-geoip

This version was forked from there and uses data from db-ip.com.

License
-------

There are two licenses for the code and data.  See the LICENSE.
